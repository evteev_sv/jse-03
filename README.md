<h1> Проект task-manager</h1>
Пример консольной программы
<br/>
<h2>1. Требования к окружению</h2>
JDK (OpenJDK) v.11
<br/>
IntelliJ Idea v. 2020.1
<h2>2. Технология</h2>
Apache Maven v.3.0
<h2>3. Данные разработчика</h2>
Евтеев Сергей
<br/>
email: [sergey@evteev.com](url)
<h2>4. Сборка приложения</h2>

```
mvn clean install
```

<h2>5. Запуск приложения</h2>

```
java -jar task-manager.jar
```