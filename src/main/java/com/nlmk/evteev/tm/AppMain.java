package com.nlmk.evteev.tm;

import static com.nlmk.evteev.tm.util.TerminalConst.*;

/**
 * Основной класс
 */
public class AppMain {

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        switch (param)
        {
            case CMD_VERSION:
                displayVersion();
            case CMD_HELP:
                displayHelp();
            case CMD_ABOUT:
                displayAbout();
            default:
                displayError();
        }
    }

    /**
     * Показ сообщения об ошибке
     */
    private static void displayError() {
        System.out.println("Not supported arguments. Type key help to list avialable argumets...");
    }

    /**
     * Показ сведений о ключах
     */
    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

    /**
     * Показ сведений о версиях
     */
    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    /**
     * Показ сведений об авторе
     */
    private static void displayAbout() {
        System.out.println("Author: Sergey Evteev");
        System.out.println("e-mail: sergey@evteev.ru");
        System.exit(0);
    }

    /**
     * Показ приветствия
     */
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

}
